package br.com.userede.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Funcionario extends Pessoa {

    public String comissao;
    public String carteiraTrabalho;

    private static List<Funcionario> funcionariosLista = new ArrayList<>();

    public Funcionario() {
    }

    ;

    public Funcionario(String nome, String cpf, String telefone, String comissao, String carteiraTrabalho) {
        super(nome, cpf, telefone);
        this.carteiraTrabalho = carteiraTrabalho;
        this.comissao = comissao;
    }

    public String getComissao() {
        return comissao;
    }

    public String getCarteiraTrabalho() {
        return carteiraTrabalho;
    }

    public void setSalario(String comissao) {
        this.comissao = comissao;
    }

    public void setCarteiraTrabalho(String carteiraTrabalho) {
        this.carteiraTrabalho = carteiraTrabalho;
    }


    //Método que adiciona Funcionário na lista.
    public static void adicionarFuncionario(Funcionario funcionario) {

        funcionariosLista.add(funcionario);

    }

    //Método que mostra o Funcionário na lista.
    public static void mostrarFuncionario(Funcionario funcionario) {
        System.out.println("         Funcionário cadastrados");
        for (Pessoa percorrerfuncionarios : funcionariosLista) {
            System.out.println(percorrerfuncionarios);
        }

    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("\n|===================================================|");
        string.append("\n O nome do funcionário é: " + getNome());
        string.append("\n O CPF do funcionário é: " + getCpf());
        string.append("\n O telefone do funcionário é: " + getTelefone());
        string.append("\n A comissão do funcionário é: " + comissao);
        string.append("\n O código do funcionário é: " + carteiraTrabalho);
        string.append("\n|===================================================|");
        return string.toString();
    }


}
