package br.com.userede.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Morador extends Pessoa {

    private String tempoContrato;


    public Morador() {
    }


    public Morador(String nome, String cpf, String telefone, String tempoContrato) {
        super(nome, cpf, telefone);
        this.tempoContrato = tempoContrato;
    }


    public String getTempoContrato() {
        return tempoContrato;
    }

    public void setTempoContrato(String tempoContrato) {
        this.tempoContrato = tempoContrato;
    }
    @Override
    public String toString() {

        StringBuilder string = new StringBuilder();
        string.append("\n|===================================================|");
        string.append("\n O nome do Morador é: " + getNome());
        string.append("\n O CPF do Morador é: " + getCpf());
        string.append("\n O telefone do Morador é: " + getTelefone());
        string.append("\n O tempo do contrato é: " + tempoContrato);
        string.append("\n|===================================================|");
        return string.toString();
    }

}










