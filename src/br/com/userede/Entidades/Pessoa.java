package br.com.userede.Entidades;

public abstract class Pessoa {
    private String nome;
    private String cpf;
    private String telefone;


    public Pessoa() {

    }

    public Pessoa(String nome, String cpf, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;


    }


    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getTelefone() {
        return telefone;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCPF(String cpf) {
        this.cpf = cpf;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("Nome:" + nome);
        string.append("\n CPF" + cpf);
        string.append("\n OTelefone:" + telefone);
        return string.toString();
    }

}



























