package br.com.userede.Entidades;

import java.util.ArrayList;
import java.util.List;

public class Imovel {

    public String codImovel;
    public String tipoImovel;
    public String qtdComodo;
    public String endereco;
    public String valorAluguel;
    public Funcionario funcionario;


    private static List<Morador> moradoresLista = new ArrayList<>();


    public Imovel() {

    }


    public Imovel(String codImovel, String tipoImovel, String qtdComodo, String endereco, String valorAluguel, Funcionario funcionario) {
        this.codImovel = codImovel;
        this.tipoImovel = tipoImovel;
        this.qtdComodo = qtdComodo;
        this.endereco = endereco;
        this.valorAluguel = valorAluguel;
        this.funcionario = funcionario;

    }

    public String getCodImovel() {
        return codImovel;
    }

    public String getTipoImovel() {
        return tipoImovel;
    }

    public String getQtdComodo() {
        return qtdComodo;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getValor() {
        return valorAluguel;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setCodImovel(String codImovel) {
        this.codImovel = codImovel;
    }

    public void setTipoImovel(String tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public void setQtdComodo(String qtdComodo) {
        this.qtdComodo = qtdComodo;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setValor(String valorAluguel) {
        this.valorAluguel = valorAluguel;
    }


    //Método  responsável pela adição dos  moradores na lista.
    public static void adicionarMoradoresLista(Morador moradores) throws Exception {
        validarCpfMorador(moradores.getCpf());
        moradoresLista.add(moradores);
        System.out.println("Morador adicionado com sucesso.");
    }

    //Método responsável por mostrar os dados na lista.
    public static void mostrarLitaMoradores() {
        System.out.println("Moradores dos Imóveis.");

        for (Pessoa percorrerMoradoresLista : moradoresLista) {
            System.out.println(percorrerMoradoresLista);

        }
    }


    //Método responsável pela exclusão do Morador.
    public static void excluirMorador(String cpf, String endereco) {

        boolean cpfExclusao = false;
        boolean enderecoExclusao = false;
        if (enderecoExclusao = false) {
            Imovel enderecoDeletar = null;


            for (Imovel percorreLista : Catalogo.imoveisLista) {
                if (percorreLista.getEndereco().equals(endereco)) {
                    System.out.println("Morador foi Excluido com sucesso");
                    cpfExclusao = true;
                    enderecoDeletar = percorreLista;
                    enderecoExclusao = true;
                }
            }
        }
        if (cpfExclusao = true) {
            Morador objMorador = null;
            for (Morador percorrelista : moradoresLista) {
                if (percorrelista.getCpf().equals(cpf)) {
                    System.out.println("Morador Excluido");
                    objMorador = percorrelista;
                    cpfExclusao = true;
                }
            }
            moradoresLista.remove(objMorador);
            System.out.println("Morador foi Excluido com sucesso");
        }

    }


    //Método que faz a validação do CPF.
    public static void validarCpfMorador(String cpf) throws Exception {

        for (Morador morador : moradoresLista) {
            if (morador.getCpf().equals(cpf)) {
                throw new Exception("CPF já cadastrado não foi possível cadastrar o Morador");

            }
            Sistema.execultarSistema();
        }

    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("\n|==================================================|");
        string.append("\n O código do Imóvel é:" + codImovel);
        string.append("\n O tipo do Imóvel: " + tipoImovel);
        string.append("\n O Imóvel tem " + qtdComodo + " comodos ");
        string.append("\n O endereço do Imóvel é " + endereco);
        string.append("\n O Valor do Alugel é RS: " + valorAluguel);
        string.append("\n Dados dos Funcionarios " + funcionario);
        string.append("\n  Dados dos Moradores   " + moradoresLista);
        string.append("\n|===================================================|");
        return string.toString();
    }

}



