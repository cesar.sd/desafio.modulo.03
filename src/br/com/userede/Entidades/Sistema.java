package br.com.userede.Entidades;


import java.sql.SQLOutput;
import java.util.Scanner;


public class Sistema {

    //Método de entrada da Classe Imóvel.
    public static void cadastrarImovel() throws Exception {
        IO.mostraTexto("|======================|");
        IO.mostraTexto("| Cadastramento Imóvel |");
        IO.mostraTexto("|======================|");
        IO.mostraTexto("Digite o código do Imóvel.");
        String codImovel = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o tipo do Imóvel.");
        String tipoImovel = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite a quantidade de cômodos do Imóvel.");
        String qtdComodo = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o endereço do Imóvel.");
        String endereco = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o valor do aluguel do Imóvel.");
        String valorAluguel = IO.leituraScanner().nextLine();

        Funcionario obj_funcionario = Sistema.cadastrarFuncionario();


        Imovel imovel = new Imovel(codImovel, tipoImovel, qtdComodo, endereco, valorAluguel, obj_funcionario);

        Catalogo.adicionarImovel(imovel);


        IO.mostraTexto("Digite a quantidade de moradores do Imóvel");

        double qtdMoradoes = IO.leituraScanner().nextDouble();
        for (int contador = 0; contador <= qtdMoradoes; contador++) {
            Morador obj_Morador = cadastarMorador();
            imovel.adicionarMoradoresLista(obj_Morador);

        }


    }

    //Método de entrada para o cadastramenro de moradores.
    public static Morador cadastarMorador() throws Exception {
        IO.mostraTexto("|=======================|");
        IO.mostraTexto("| Cadastramento Morador |");
        IO.mostraTexto("|=======================|\n");
        IO.mostraTexto("Digite nome do morador");
        String nome = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o CPF do morador");
        String cpf = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o telefone do morador ");
        String telefone = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o tempo de contrato ");
        String tempoContrato = IO.leituraScanner().nextLine();

        Morador morador = new Morador(nome, cpf, telefone, tempoContrato);


        return morador;
    }

    //Método de entrada para o cadastramenro do Funcionário.
    public static Funcionario cadastrarFuncionario() {
        IO.mostraTexto("|==========================================|");
        IO.mostraTexto("| Cadastramento do Funcionário Responsável |");
        IO.mostraTexto("|==========================================|");
        IO.mostraTexto("Nome do Funcionário.");
        String nome = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o CPF do Funcionário.");
        String cpf = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o telefone do Funcionário.");
        String telefone = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite a comissão do Funcionário.");
        String comissao = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o código do Funcionário.");
        String carteiraTrabalho = IO.leituraScanner().nextLine();

        Funcionario funcionario = new Funcionario(nome, cpf, telefone, comissao, carteiraTrabalho);
        Funcionario.adicionarFuncionario(funcionario);

        return funcionario;
    }

    //Método de entrada para o método Exclução.
    public static void excluirMoradorPorEmailComEndereco() {
        IO.mostraTexto("|===========================|");
        IO.mostraTexto("|      Excluir Morador      |");
        IO.mostraTexto("|===========================|\n");
        IO.mostraTexto("Digite o CPF do morador para excluir");
        String cpf = IO.leituraScanner().nextLine();
        IO.mostraTexto("Digite o endereço do Imóvel em que o morador está cadastrado");
        String endereco = IO.leituraScanner().nextLine();

        Imovel.excluirMorador(cpf, endereco);


    }

    //Método com as alternativa visuais do menu.
    public static void menu() {
        System.out.println("\n|============================================|");
        System.out.println("|========== Sistema Imobiliario =============|");
        System.out.println("|============================================|");
        System.out.println("|  1- Cadastrar um Imóvel.                   |");
        System.out.println("|  2- Mostrar Imóveis cadastrados.           |");
        System.out.println("|  3- Mostrar Moradores cadastrados.         |");
        System.out.println("|  4- Mostrar Funcionarios cadastrados.      |");
        System.out.println("|  5- Excluir.                               |");
        System.out.println("|  6- Sair do Sistema.                       |");
        System.out.println("|============================================|");


    }

    //Método com as opções de execução do menu.
    public static void execultarSistema() throws Exception {
        boolean executar = true;
        Funcionario obj_funcionario = new Funcionario();
        while (executar == true) {
            menu();
            String OpcaoEscolhida = IO.leituraScanner().next();

            switch (OpcaoEscolhida) {
                case "1":
                    cadastrarImovel();
                    break;
                case "2":
                    Catalogo.mostrarListaImoveis();
                    break;
                case "3":
                    Imovel.mostrarLitaMoradores();
                    break;
                case "4":
                    Funcionario.mostrarFuncionario(obj_funcionario);
                    break;
                case "5":
                    excluirMoradorPorEmailComEndereco();
                    break;
                case "6":
                    executar = false;
                    break;

            }

        }

    }


}
